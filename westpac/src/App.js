import React, { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { loadData } from './store/actions';
import EmployeeListing from './componants/EmployeeListing';

function App() {
  const dispatch = useDispatch();
  useEffect(() => {
    fetch('./sample-data.json')
      .then(res => res.json())
      .then(data => {
        dispatch(loadData(data))
      })
      .catch(err => console.log("error in load data :", err))
  }, [dispatch])
  return (
    <div className="App">
      <EmployeeListing />
    </div>
  );
}

export default App;
