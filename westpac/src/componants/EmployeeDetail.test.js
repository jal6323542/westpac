import { render, screen, waitFor, fireEvent, expect } from '@testing-library/react';
import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';
import EmployeeDetail from './EmployeeDetail';

const mockStore = configureStore([]);

const initialState = {
    companyInfo: {
        companyName: "Mayer, Monahan and Christiansen",
        companyMotto: "virtual mesh e-commerce",
        companyEst: "2021-01-24T11:39:38.010Z"
    },
    employees: [
        {
            id: "6c35f8e6-ba2b-4007-92c3-ccd8d7ff3fe1",
            avatar: "https://picsum.photos/200/300",
            firstName: "Grace",
            lastName: "Ritchie",
            jobTitle: "Principal Brand Facilitator",
            contactNo: "0461 681 869",
            address: "Bartell Crest Sophieview, Queensland",
            age: 47,
            bio: "Maiores vel recusandae numquam voluptate vitae. Asperiores aliquam ab. Et consequatur quia aut sint. Dolorem quo corporis dolor. Repellendus rem quia enim nesciunt. Architecto ipsam error et libero.",
            dateJoined: "2020-12-25T07:17:39.639Z"
        }
    ],
}

test('renders the EmployeeDetail componant and check Employee detail data when `open` is set to true', () => {
    const store = mockStore(initialState);


    const selectedRowData = {
        id: "6c35f8e6-ba2b-4007-92c3-ccd8d7ff3fe1",
        avatar: "https://picsum.photos/200/300",
        firstName: "Grace",
        lastName: "Ritchie",
        jobTitle: "Principal Brand Facilitator",
        contactNo: "0461 681 869",
        address: "Bartell Crest Sophieview, Queensland",
        age: 47,
        bio: "Maiores vel recusandae numquam voluptate vitae. Asperiores aliquam ab. Et consequatur quia aut sint. Dolorem quo corporis dolor. Repellendus rem quia enim nesciunt. Architecto ipsam error et libero.",
        dateJoined: "2020-12-25T07:17:39.639Z"
    }

    render(
        <Provider store={store}>
            <EmployeeDetail
                selectedRowData={selectedRowData}
                open={true}
            />
        </Provider>
    );

    const employeeBioElement = screen.getByText('Maiores vel recusandae numquam voluptate vitae. Asperiores aliquam ab. Et consequatur quia aut sint. Dolorem quo corporis dolor. Repellendus rem quia enim nesciunt. Architecto ipsam error et libero.');
    const employeeFullName = screen.getByText('Grace Ritchie');


    waitFor(() => {

        expect(employeeBioElement).toBeInTheDocument();
        expect(employeeFullName).toBeInTheDocument();

    });


});

test(' EmployeeDetail componant will not render when `open` prop is set to false', () => {
    const store = mockStore(initialState);


    const selectedRowData = {
        id: "6c35f8e6-ba2b-4007-92c3-ccd8d7ff3fe1",
        avatar: "https://picsum.photos/200/300",
        firstName: "Grace",
        lastName: "Ritchie",
        jobTitle: "Principal Brand Facilitator",
        contactNo: "0461 681 869",
        address: "Bartell Crest Sophieview, Queensland",
        age: 47,
        bio: "Maiores vel recusandae numquam voluptate vitae. Asperiores aliquam ab. Et consequatur quia aut sint. Dolorem quo corporis dolor. Repellendus rem quia enim nesciunt. Architecto ipsam error et libero.",
        dateJoined: "2020-12-25T07:17:39.639Z"
    }

    render(
        <Provider store={store}>
            <EmployeeDetail
                selectedRowData={selectedRowData}
                open={false}
            />
        </Provider>
    );



    waitFor(() => {

        expect(() => getByText('Maiores vel recusandae numquam voluptate vitae. Asperiores aliquam ab. Et consequatur quia aut sint. Dolorem quo corporis dolor. Repellendus rem quia enim nesciunt. Architecto ipsam error et libero.')).toThrow('Unable to find an element');


    });


});


