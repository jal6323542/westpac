import React, { useEffect, useState, useRef } from 'react';
import { useSelector } from 'react-redux';
import { DataGrid } from '@mui/x-data-grid';
import { Typography } from '@mui/material';
import TextField from '@mui/material/TextField';
import { styled } from '@mui/material/styles';
import Box from '@mui/material/Box';
import Paper from '@mui/material/Paper';
import Grid from '@mui/material/Grid';
import Container from '@mui/material/Container';
import { Divider } from '@mui/material';
import './employee-listing.css';
import EmployeeDetail from './EmployeeDetail';

const EmployeeListing = () => {
    const [employeeList, setEmployeeList] = useState([]);
    const [selectedRowData, setSelectedRowData] = useState({});
    const [open, setOpen] = useState(false);
    const employeeState = useSelector((state) => state.employees);
    const companyInfo = useSelector((state) => state.companyInfo);
    const input = useRef(null);
    useEffect(() => {
        if (employeeState.length > 0) {
            setEmployeeList(employeeState);

        }

    }, [employeeState]);



    const searchHandler = () => {
        const searchedVal = input.current.value;
        const filteredRows = employeeState.filter((row) => {
            const fullName = row.firstName + '' + row.lastName;
            return fullName.toLowerCase().includes(searchedVal.toLowerCase());
        });
        setEmployeeList(filteredRows);
    }

    const employeeDetailHandler = (rowData) => {
        setSelectedRowData(rowData.row);
        setOpen(true)
    }


    const columns = [
        {
            field: 'id',
            headerName: 'ID',
            width: 300,
            sortable: true,
        },
        {
            field: 'fullName',
            headerName: 'Full name',
            description: 'This column has a value getter and is not sortable.',
            sortable: true,
            width: 160,
            valueGetter: (params) =>
                `${params.row.firstName || ''} ${params.row.lastName || ''}`,
        },

        {
            field: 'contactNo',
            headerName: 'Contact No',
            width: 130,
            sortable: true,
        },
        {
            field: 'address',
            headerName: 'Address',
            width: 500,
            sortable: true,
        },


    ];


    return (

        <Box style={{ border: '1px solid lightgray', margin: "1%" }} sx={{ flexGrow: 1, padding: '5%' }}>
            <Grid container spacing={2}>
                <Grid item xs={12}>
                    <Typography variant="h5" >
                        {companyInfo ? companyInfo.companyName : "No Company"}
                    </Typography>
                </Grid>
                <Grid item xs={9}>
                    <Typography variant="caption"  >
                        {companyInfo ? companyInfo.companyMotto : "No Company Motto"}
                    </Typography>
                </Grid>
                <Grid item xs={3}>
                    <Typography variant="caption" sx={{ float: "right" }}>
                        {`Since(${companyInfo ? new Date(companyInfo.companyEst).toLocaleDateString("en-US") : ""})`}
                    </Typography>
                </Grid>
                <Grid item xs={9} >
               
                </Grid>
              
                <Grid item xs={3} >
                
                    <input type='text' className='input' ref={input} />
                    <button className=' button' onClick={searchHandler} >
                        Search
                    </button>

                </Grid>

                <Grid item xs={12}>
               
                    <DataGrid
                        rows={employeeList}
                        columns={columns}
                        onRowClick={(row) => employeeDetailHandler(row)}
                        initialState={{
                            pagination: {
                                paginationModel: { page: 0, pageSize: 5 },
                            },
                        }}
                        pageSizeOptions={[5, 10]}

                    />
                </Grid>


            </Grid>
            <EmployeeDetail selectedRowData={selectedRowData} open={open} setOpen={setOpen} />
        </Box>

    )

}
export default EmployeeListing;