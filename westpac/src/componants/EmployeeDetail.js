import * as React from 'react';
import Button from '@mui/material/Button';
import { styled } from '@mui/material/styles';
import Dialog from '@mui/material/Dialog';
import DialogTitle from '@mui/material/DialogTitle';
import DialogContent from '@mui/material/DialogContent';
import DialogActions from '@mui/material/DialogActions';
import IconButton from '@mui/material/IconButton';
import CloseIcon from '@mui/icons-material/Close';
import Typography from '@mui/material/Typography';
import Grid from '@mui/material/Grid';
import CardMedia from '@mui/material/CardMedia';

const BootstrapDialog = styled(Dialog)(({ theme }) => ({
    '& .MuiDialogContent-root': {
        padding: theme.spacing(2),
    },
    '& .MuiDialogActions-root': {
        padding: theme.spacing(1),
    },
}));

export default function EmployeeDetail({ setOpen, open, selectedRowData }) {
    const handleClose = () => {
        setOpen(false);
    };

    return (
        <div>

            <BootstrapDialog
                onClose={handleClose}
                aria-labelledby="customized-dialog-title"
                open={open}
            >

                <IconButton
                    aria-label="close"
                    onClick={handleClose}
                    sx={{
                        position: 'absolute',
                        right: 8,
                        top: 8,
                        color: (theme) => theme.palette.grey[500],
                    }}
                >
                    <CloseIcon />
                </IconButton>
                <DialogContent dividers>
                    <Grid container spacing={3}>
                        <Grid item xs={3}>
                            <img
                                width='70'
                                height='70'
                                src={selectedRowData.avatar}
                                alt="Live from space album cover"
                            />
                             <br></br>
                            <Typography variant='caption' fontSize={9} gutterBottom>
                                {selectedRowData ? selectedRowData.jobTitle :""}
                            </Typography>
                            <br></br>
                            <Typography variant='caption' fontSize={9} gutterBottom>
                                {selectedRowData ? selectedRowData.age :""}
                            </Typography>
                            <br></br>
                            <Typography variant='caption' fontSize={9} gutterBottom>
                                {selectedRowData ?  new Date(selectedRowData.dateJoined).toLocaleDateString("en-US") :""}
                            </Typography>
                        </Grid>
                        <Grid item xs={9} sx={{marginTop:"10%"}}>
                            <Typography variant='h6' gutterBottom>
                                {selectedRowData ? selectedRowData.firstName + " " + selectedRowData.lastName : ""}
                            </Typography>
                            <hr></hr>
                            
                            <Typography variant='caption' gutterBottom>
                                {selectedRowData ? selectedRowData.bio :""}
                            </Typography>
                        </Grid>
                        
                    </Grid>
                   
                </DialogContent>
                
            </BootstrapDialog>
        </div>
    );
}