import { render, screen, waitFor, fireEvent, expect } from '@testing-library/react';
import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';
import EmployeeListing from './EmployeeListing';
const mockStore = configureStore([]);

const initialState = {
    companyInfo: {
        companyName: "Mayer, Monahan and Christiansen",
        companyMotto: "virtual mesh e-commerce",
        companyEst: "2021-01-24T11:39:38.010Z"
    },
    employees: [
        {
            id: "6c35f8e6-ba2b-4007-92c3-ccd8d7ff3fe1",
            avatar: "https://picsum.photos/200/300",
            firstName: "Grace",
            lastName: "Ritchie",
            jobTitle: "Principal Brand Facilitator",
            contactNo: "0461 681 869",
            address: "Bartell Crest Sophieview, Queensland",
            age: 47,
            bio: "Maiores vel recusandae numquam voluptate vitae. Asperiores aliquam ab. Et consequatur quia aut sint. Dolorem quo corporis dolor. Repellendus rem quia enim nesciunt. Architecto ipsam error et libero.",
            dateJoined: "2020-12-25T07:17:39.639Z"
        }
    ],
}

test('renders the EmployeeListing componant and check Employee data', () => {
    const store = mockStore(initialState);

    render(
        <Provider store={store}>
            <EmployeeListing />
        </Provider>
    );

    const companyNameElement = screen.getByText('Mayer, Monahan and Christiansen');
    const companyMottoElement = screen.getByText('virtual mesh e-commerce');
    const employeeFullName = screen.getByText('Grace Ritchie');


    waitFor(() => {

        expect(companyMottoElement).toBeInTheDocument();
        expect(employeeFullName).toBeInTheDocument();
        expect(companyNameElement).toBeInTheDocument();
    });


});

test(' check Search button is present or not', () => {
    const store = mockStore(initialState);

    render(
        <Provider store={store}>
            <EmployeeListing />
        </Provider>
    );

    const isSearchButton = screen.getByRole('button', { name: /Search/i });
    waitFor(() => {
        expect(isSearchButton).toBeInTheDocument();
    })


});

test(' click on any row and then check `row selected` text', () => {
    const store = mockStore(initialState);

    render(
        <Provider store={store}>
            <EmployeeListing />
        </Provider>
    );

    const employeeFullName = screen.getByText('Grace Ritchie');
    
    waitFor(() => {
        expect(employeeFullName).toBeInTheDocument();
        fireEvent.click(employeeFullName);
        expect(getByText('row selected')).toBeInTheDocument()
    })


});
