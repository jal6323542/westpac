const initialState = {
    companyInfo: {},
    employees: [],
}

const rootReducer = (state = initialState, action) => {
    switch (action.type) {
        case 'LOAD_DATA':
            return { ...state, ...action.data };
        default:
            return state;
    }
};

export default rootReducer;